NOT_A_PLATE = "Impossible to create a plate with those characters";
VALID_PLATE = "Valid Plate";

function isValidPlate(plate) {
  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;
  
  type = "";

  if(plate.match(re) != null) 
  {
  	type = VALID_PLATE;
  } 
  else
  {
  	type = NOT_A_PLATE;
  }
  return type;
}