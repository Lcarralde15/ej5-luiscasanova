test("Valid Plate", function (assert) {
    assert.equal(isValidPlate('1234BCD'), VALID_PLATE, "VALID");
    assert.equal(isValidPlate('8095ZRT'), VALID_PLATE, "VALID");
    assert.equal(isValidPlate('7402PLD'), VALID_PLATE, "VALID");
});

test("Not a Plate", function (assert) {
    assert.equal(isValidPlate('1111ABC'), NOT_A_PLATE, "CHARACTER INVALID USING VOWELS");
    assert.equal(isValidPlate('413QEÑ'), NOT_A_PLATE, "CHARACTER INVALID USING Ñ");
    assert.equal(isValidPlate('1379GQY'), NOT_A_PLATE, "CHARACTER INVALID USING Q");
    assert.equal(isValidPlate('9754-RTC'), NOT_A_PLATE, "CHARACTER INVALID USING PUNTUATION");
    assert.equal(isValidPlate('3456:WRV'), NOT_A_PLATE, "CHARACTER INVALID USING PUNTUATION");
    assert.equal(isValidPlate('1 37  9 RBM'), NOT_A_PLATE, "CHARACTER INVALID USING SPACES");
    assert.equal(isValidPlate('159HRT'), NOT_A_PLATE, "NOT ENOUGH DIGITS");
    assert.equal(isValidPlate('8763KR'), NOT_A_PLATE, "NOT ENOUGH LETTERS");
    assert.equal(isValidPlate('246YT'), NOT_A_PLATE, "NOT ENOUGH DIGITS AND LETTERS");
    assert.equal(isValidPlate('1234567'), NOT_A_PLATE, "ONLY NUMBERS");
    assert.equal(isValidPlate('GHKTRSP'), NOT_A_PLATE, "ONLY LETTERS");
    assert.equal(isValidPlate('PKR6845'), NOT_A_PLATE, "NOT ORDERED PLATE");
    assert.equal(isValidPlate('1B2C3D4'), NOT_A_PLATE, "NOT ORDERED PLATE");
});
